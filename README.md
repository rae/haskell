Typing rules for Haskell
========================

In the course of writing GHC, it frequently comes to pass that we
need proper typing rules for a snippet of Haskell. These will be
written here, using [ott](https://www.cl.cam.ac.uk/~pes20/ott/).

The plan for fleshing all this out lives in [the wiki](https://gitlab.haskell.org/rae/haskell/-/wikis/Project-plan).

You can access a [built version of this PDF](https://gitlab.haskell.org/rae/haskell/-/jobs/artifacts/master/raw/haskell.pdf?job=build-pdf),
though that link only works roughly 30 days from the last push (it points to a CI artifact
that gets cleared out).
