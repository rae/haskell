MAIN = haskell
OTTFILES = grammar.ott rules.ott
MNGFILES = haskell.mng

OTT_OPTS = -tex_show_meta false -tex_wrap false -picky_multiple_parses false
OTT_TEX = ott.tex

all: $(MAIN).pdf

ott: ottall.pdf

%.pdf: %.tex $(OTT_TEX)
	latexmk -pdf $*

%.tex: %.mng $(OTTFILES)
	rm -f $@
	ott $(OTT_OPTS) -tex_filter $< $@ $(OTTFILES)
	chmod -w $@

$(OTT_TEX): $(OTTFILES)
	rm -f $@
	ott $(OTT_OPTS) -o $@ $^
	chmod -w $@

clean:
	rm -f *.aux *.log *.bbl *.blg *.ptb *~
	rm -f *.fdb_latexmk *.fls *.out
	rm -f comment.cut *.o *.hi
	rm -f $(MAIN).pdf $(OTT_TEX) ottall.pdf
	rm -f $(MNGFILES:%.mng=%.tex)

.PHONY: all clean ott
.SECONDARY:
